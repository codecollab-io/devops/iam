terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "3.81.0"
    }
  }
}

variable "GCP_SERVICE_ACCOUNT" {
  type = string
}

variable "GCP_SERVICE_KEY" {
  type = string
}

provider "google" {
  project     = "codecollab-io"
  region      = "us-central1"
  credentials = var.GCP_SERVICE_KEY
}

provider "google-beta" {
  project     = "codecollab-io"
  region      = "us-central1"
  credentials = var.GCP_SERVICE_KEY
}

data "google_project" "project" {}

data "terraform_remote_state" "secret_manager" {
  backend = "remote"

  config = {
    organization = "codecollab-io"
    workspaces   = {
      name = "secret-manager-production"
    }
  }
}

data "terraform_remote_state" "cloud_run" {
  backend = "remote"

  config = {
    organization = "codecollab-io"
    workspaces   = {
      name = "cloud-run-production"
    }
  }
}

resource "google_billing_account_iam_member" "yu_hsuan" {
    billing_account_id = "01491C-D18B61-B571E6"
    member = "user:officialfishypower@gmail.com"
    role = "roles/billing.viewer"
}

resource "google_project_iam_member" "yu_hsuan" {
  member = "user:officialfishypower@gmail.com"
  role = "roles/viewer"
}

resource "google_project_iam_member" "rui_yang" {
    member = "user:thinkerpal@codecollab.io"
    role = "roles/viewer"
}

resource "google_project_iam_member" "sean_appengine_appAdmin" {
    member = "user:derpydm@gmail.com"
    role = "roles/appengine.appAdmin"
}

resource "google_project_iam_member" "sean_compute_admin" {
    member = "user:derpydm@gmail.com"
    role = "roles/compute.admin"
}

//          $$\   $$\     $$\           $$\
//          \__|  $$ |    $$ |          $$ |
// $$$$$$\  $$\ $$$$$$\   $$ | $$$$$$\  $$$$$$$\
//$$  __$$\ $$ |\_$$  _|  $$ | \____$$\ $$  __$$\
//$$ /  $$ |$$ |  $$ |    $$ | $$$$$$$ |$$ |  $$ |
//$$ |  $$ |$$ |  $$ |$$\ $$ |$$  __$$ |$$ |  $$ |
//\$$$$$$$ |$$ |  \$$$$  |$$ |\$$$$$$$ |$$$$$$$  |
// \____$$ |\__|   \____/ \__| \_______|\_______/
//$$\   $$ |
//\$$$$$$  |
// \______/

# Create service account for GitLab
resource "google_service_account" "gitlab_ci" {
  account_id   = "gitlab-ci"
  display_name = "GitLab CI"
  description  = " Grants the GitLab CI runner access to Artifact Registry and Cloud Run"
}

# Create key for service account
resource "google_service_account_key" "gitlab_ci" {
  service_account_id = google_service_account.gitlab_ci.account_id
}

# Add Cloud Run CI role to service account
resource "google_project_iam_member" "gitlab_ci_run" {
  member = "serviceAccount:${google_service_account.gitlab_ci.email}"
  role   = "projects/codecollab-io/roles/run.ci"
}

# Add Artifact Registry CI role to service account
resource "google_project_iam_member" "gitlab_ci_artifactregistry" {
  member = "serviceAccount:${google_service_account.gitlab_ci.email}"
  role   = "projects/codecollab-io/roles/artifactregistry.ci"
}

# Add Compute Engine Instance Admin v1 for Packer to run
resource "google_project_iam_member" "gitlab_ci_compute_instance_admin" {
  member = "serviceAccount:${google_service_account.gitlab_ci.email}"
  role   = "roles/compute.instanceAdmin.v1"
}

# Allow access to cc-compiler-servant-qa config
resource "google_secret_manager_secret_iam_member" "gitlab_ci_cc_compiler_servant_qa_config" {
  provider = google-beta

  secret_id = data.terraform_remote_state.secret_manager.outputs.cc_compiler_servant_qa_config_secret_id
  role      = "roles/secretmanager.secretAccessor"
  member    = "serviceAccount:${google_service_account.gitlab_ci.email}"
}

# Allow access to cc-compiler-servant-us config
resource "google_secret_manager_secret_iam_member" "gitlab_ci_cc_compiler_servant_us_config" {
  provider = google-beta

  secret_id = data.terraform_remote_state.secret_manager.outputs.cc_compiler_servant_us_config_secret_id
  role      = "roles/secretmanager.secretAccessor"
  member    = "serviceAccount:${google_service_account.gitlab_ci.email}"
}

# Allow access to cc-compiler-servant-sg config
resource "google_secret_manager_secret_iam_member" "gitlab_ci_cc_compiler_servant_sg_config" {
  provider = google-beta

  secret_id = data.terraform_remote_state.secret_manager.outputs.cc_compiler_servant_sg_config_secret_id
  role      = "roles/secretmanager.secretAccessor"
  member    = "serviceAccount:${google_service_account.gitlab_ci.email}"
}

# Allow access to cc-compiler-servant-eu config
resource "google_secret_manager_secret_iam_member" "gitlab_ci_cc_compiler_servant_eu_config" {
  provider = google-beta

  secret_id = data.terraform_remote_state.secret_manager.outputs.cc_compiler_servant_eu_config_secret_id
  role      = "roles/secretmanager.secretAccessor"
  member    = "serviceAccount:${google_service_account.gitlab_ci.email}"
}
data "google_app_engine_default_service_account" "default" {}

# Allow access to App Engine Deployer
resource "google_project_iam_member" "gitlab_ci_app_engine_app_deployer" {
  member = "serviceAccount:${google_service_account.gitlab_ci.email}"
  role   = "roles/appengine.deployer"
}

resource "google_project_iam_member" "gitlab_ci_app_engine_service_admin" {
  member = "serviceAccount:${google_service_account.gitlab_ci.email}"
  role   = "roles/appengine.serviceAdmin"
}

resource "google_project_iam_member" "gitlab_ci_cloud_build_editor" {
  member = "serviceAccount:${google_service_account.gitlab_ci.email}"
  role   = "roles/cloudbuild.builds.editor"
}

resource "google_project_iam_member" "gitlab_ci_cloud_object_admin" {
  member = "serviceAccount:${google_service_account.gitlab_ci.email}"
  role   = "roles/storage.objectAdmin"
}

resource "google_project_iam_member" "gitlab_ci_vpc_access_user" {
  member = "serviceAccount:${google_service_account.gitlab_ci.email}"
  role   = "roles/vpcaccess.user"
}

resource "google_project_iam_member" "gitlab_ci_kubernetes_cluster_admin" {
  member = "serviceAccount:${google_service_account.gitlab_ci.email}"
  role   = "roles/container.clusterAdmin"
}

resource "google_service_account_iam_member" "gitlab_ci_app_engine_default_service_account" {
  service_account_id = data.google_app_engine_default_service_account.default.name
  role               = "roles/iam.serviceAccountUser"
  member             = "serviceAccount:${google_service_account.gitlab_ci.email}"
}

//                                                                  $$\
//                                                                  \__|
// $$$$$$\   $$$$$$\   $$$$$$\         $$$$$$\  $$$$$$$\   $$$$$$\  $$\ $$$$$$$\   $$$$$$\
// \____$$\ $$  __$$\ $$  __$$\       $$  __$$\ $$  __$$\ $$  __$$\ $$ |$$  __$$\ $$  __$$\
// $$$$$$$ |$$ /  $$ |$$ /  $$ |      $$$$$$$$ |$$ |  $$ |$$ /  $$ |$$ |$$ |  $$ |$$$$$$$$ |
//$$  __$$ |$$ |  $$ |$$ |  $$ |      $$   ____|$$ |  $$ |$$ |  $$ |$$ |$$ |  $$ |$$   ____|
//\$$$$$$$ |$$$$$$$  |$$$$$$$  |      \$$$$$$$\ $$ |  $$ |\$$$$$$$ |$$ |$$ |  $$ |\$$$$$$$\
// \_______|$$  ____/ $$  ____/        \_______|\__|  \__| \____$$ |\__|\__|  \__| \_______|
//          $$ |      $$ |                                $$\   $$ |
//          $$ |      $$ |                                \$$$$$$  |
//          \__|      \__|                                 \______/

# Allow access to cc-html-server-qa config
resource "google_secret_manager_secret_iam_member" "app_engine_cc_html_server_qa_config" {
  provider = google-beta

  secret_id = data.terraform_remote_state.secret_manager.outputs.cc_html_server_qa_config_secret_id
  role      = "roles/secretmanager.secretAccessor"
  member    = "serviceAccount:${data.google_app_engine_default_service_account.default.email}"
}

# Allow access to cc-html-server-us config
resource "google_secret_manager_secret_iam_member" "app_engine_cc_html_server_us_config" {
  provider = google-beta

  secret_id = data.terraform_remote_state.secret_manager.outputs.cc_html_server_us_config_secret_id
  role      = "roles/secretmanager.secretAccessor"
  member    = "serviceAccount:${data.google_app_engine_default_service_account.default.email}"
}

# Allow access to cc-html-server-sg config
resource "google_secret_manager_secret_iam_member" "app_engine_cc_html_server_sg_config" {
  provider = google-beta

  secret_id = data.terraform_remote_state.secret_manager.outputs.cc_html_server_sg_config_secret_id
  role      = "roles/secretmanager.secretAccessor"
  member    = "serviceAccount:${data.google_app_engine_default_service_account.default.email}"
}

# Allow access to cc-html-server-eu config
resource "google_secret_manager_secret_iam_member" "app_engine_cc_html_server_eu_config" {
  provider = google-beta

  secret_id = data.terraform_remote_state.secret_manager.outputs.cc_html_server_eu_config_secret_id
  role      = "roles/secretmanager.secretAccessor"
  member    = "serviceAccount:${data.google_app_engine_default_service_account.default.email}"
}

//          $$\                           $$\
//          $$ |                          $$ |
// $$$$$$$\ $$ | $$$$$$\  $$\   $$\  $$$$$$$ |       $$$$$$\  $$\   $$\ $$$$$$$\
//$$  _____|$$ |$$  __$$\ $$ |  $$ |$$  __$$ |      $$  __$$\ $$ |  $$ |$$  __$$\
//$$ /      $$ |$$ /  $$ |$$ |  $$ |$$ /  $$ |      $$ |  \__|$$ |  $$ |$$ |  $$ |
//$$ |      $$ |$$ |  $$ |$$ |  $$ |$$ |  $$ |      $$ |      $$ |  $$ |$$ |  $$ |
//\$$$$$$$\ $$ |\$$$$$$  |\$$$$$$  |\$$$$$$$ |      $$ |      \$$$$$$  |$$ |  $$ |
// \_______|\__| \______/  \______/  \_______|      \__|       \______/ \__|  \__|


resource "google_service_account" "cloud_run" {
  account_id   = "cloud-run"
  display_name = "Cloud Run"
  description  = "Service account for Cloud Run"
}

resource "google_project_iam_member" "cloud_run_invoker" {
  role    = "roles/run.invoker"
  member  = "serviceAccount:${google_service_account.cloud_run.email}"
}

# Allow authenticated calls to other services
resource "google_cloud_run_service_iam_member" "cloud_run_cc_api_invoker" {
  service = data.terraform_remote_state.cloud_run.outputs.cc_api_name
  location = data.terraform_remote_state.cloud_run.outputs.cc_api_location

  role    = "roles/run.invoker"
  member  = "serviceAccount:${google_service_account.cloud_run.email}"
}

resource "google_cloud_run_service_iam_member" "cloud_run_cc_api_qa_invoker" {
  service = data.terraform_remote_state.cloud_run.outputs.cc_api_qa_name
  location = data.terraform_remote_state.cloud_run.outputs.cc_api_qa_location

  role    = "roles/run.invoker"
  member  = "serviceAccount:${google_service_account.cloud_run.email}"
}

resource "google_cloud_run_service_iam_member" "cloud_run_cc_editor_api_us_invoker" {
  service = data.terraform_remote_state.cloud_run.outputs.cc_editor_api_us_name
  location = data.terraform_remote_state.cloud_run.outputs.cc_editor_api_us_location

  role    = "roles/run.invoker"
  member  = "serviceAccount:${google_service_account.cloud_run.email}"
}

resource "google_cloud_run_service_iam_member" "cloud_run_cc_editor_api_eu_invoker" {
  service = data.terraform_remote_state.cloud_run.outputs.cc_editor_api_eu_name
  location = data.terraform_remote_state.cloud_run.outputs.cc_editor_api_eu_location

  role    = "roles/run.invoker"
  member  = "serviceAccount:${google_service_account.cloud_run.email}"
}

resource "google_cloud_run_service_iam_member" "cloud_run_cc_editor_api_sg_invoker" {
  service = data.terraform_remote_state.cloud_run.outputs.cc_editor_api_sg_name
  location = data.terraform_remote_state.cloud_run.outputs.cc_editor_api_sg_location

  role    = "roles/run.invoker"
  member  = "serviceAccount:${google_service_account.cloud_run.email}"
}

resource "google_cloud_run_service_iam_member" "cloud_run_cc_editor_api_qa_invoker" {
  service = data.terraform_remote_state.cloud_run.outputs.cc_editor_api_qa_name
  location = data.terraform_remote_state.cloud_run.outputs.cc_editor_api_qa_location

  role    = "roles/run.invoker"
  member  = "serviceAccount:${google_service_account.cloud_run.email}"
}

resource "google_cloud_run_service_iam_member" "cloud_run_cc_auth_invoker" {
  service = data.terraform_remote_state.cloud_run.outputs.cc_auth_name
  location = data.terraform_remote_state.cloud_run.outputs.cc_auth_location

  role    = "roles/run.invoker"
  member  = "serviceAccount:${google_service_account.cloud_run.email}"
}

resource "google_cloud_run_service_iam_member" "cloud_run_cc_auth_qa_invoker" {
  service = data.terraform_remote_state.cloud_run.outputs.cc_auth_qa_name
  location = data.terraform_remote_state.cloud_run.outputs.cc_auth_qa_location

  role    = "roles/run.invoker"
  member  = "serviceAccount:${google_service_account.cloud_run.email}"
}

resource "google_cloud_run_service_iam_member" "cloud_run_cc_lti_invoker" {
  service = data.terraform_remote_state.cloud_run.outputs.cc_lti_name
  location = data.terraform_remote_state.cloud_run.outputs.cc_lti_location

  role    = "roles/run.invoker"
  member  = "serviceAccount:${google_service_account.cloud_run.email}"
}

resource "google_cloud_run_service_iam_member" "cloud_run_cc_lti_qa_invoker" {
  service = data.terraform_remote_state.cloud_run.outputs.cc_lti_qa_name
  location = data.terraform_remote_state.cloud_run.outputs.cc_lti_qa_location

  role    = "roles/run.invoker"
  member  = "serviceAccount:${google_service_account.cloud_run.email}"
}

# Allow access to cc-projects-service-qa config
resource "google_secret_manager_secret_iam_member" "cloud_run_cc_projects_service_qa_config" {
  provider = google-beta

  secret_id = data.terraform_remote_state.secret_manager.outputs.cc_projects_service_qa_config_secret_id
  role      = "roles/secretmanager.secretAccessor"
  member    = "serviceAccount:${google_service_account.cloud_run.email}"
}

# Allow access to cc-projects-service config
resource "google_secret_manager_secret_iam_member" "cloud_run_cc_projects_service_config" {
  provider = google-beta

  secret_id = data.terraform_remote_state.secret_manager.outputs.cc_projects_service_config_secret_id
  role      = "roles/secretmanager.secretAccessor"
  member    = "serviceAccount:${google_service_account.cloud_run.email}"
}

# Allow access to jwt-public-key-qa
resource "google_secret_manager_secret_iam_member" "cloud_run_jwt_public_key_qa" {
  provider = google-beta

  secret_id = data.terraform_remote_state.secret_manager.outputs.jwt_public_key_qa_secret_id
  role      = "roles/secretmanager.secretAccessor"
  member    = "serviceAccount:${google_service_account.cloud_run.email}"
}

# Allow access to jwt-public-key-qa
resource "google_secret_manager_secret_iam_member" "cloud_run_jwt_private_key_qa" {
  provider = google-beta

  secret_id = data.terraform_remote_state.secret_manager.outputs.jwt_private_key_qa_secret_id
  role      = "roles/secretmanager.secretAccessor"
  member    = "serviceAccount:${google_service_account.cloud_run.email}"
}

# Allow access to jwt-public-key
resource "google_secret_manager_secret_iam_member" "cloud_run_jwt_public_key" {
  provider = google-beta

  secret_id = data.terraform_remote_state.secret_manager.outputs.jwt_public_key_secret_id
  role      = "roles/secretmanager.secretAccessor"
  member    = "serviceAccount:${google_service_account.cloud_run.email}"
}

# Allow access to jwt-private-key
resource "google_secret_manager_secret_iam_member" "cloud_run_jwt_private_key" {
  provider = google-beta

  secret_id = data.terraform_remote_state.secret_manager.outputs.jwt_private_key_secret_id
  role      = "roles/secretmanager.secretAccessor"
  member    = "serviceAccount:${google_service_account.cloud_run.email}"
}

# Allow access to cc-auth-qa config
resource "google_secret_manager_secret_iam_member" "cloud_run_cc_auth_qa_config" {
  provider = google-beta

  secret_id = data.terraform_remote_state.secret_manager.outputs.cc_auth_qa_config_secret_id
  role      = "roles/secretmanager.secretAccessor"
  member    = "serviceAccount:${google_service_account.cloud_run.email}"
}

# Allow access to cc-auth config
resource "google_secret_manager_secret_iam_member" "cloud_run_cc_auth_config" {
  provider = google-beta

  secret_id = data.terraform_remote_state.secret_manager.outputs.cc_auth_config_secret_id
  role      = "roles/secretmanager.secretAccessor"
  member    = "serviceAccount:${google_service_account.cloud_run.email}"
}

# Allow access to cc-api-gateway-qa config
resource "google_secret_manager_secret_iam_member" "cloud_run_cc_api_gateway_qa_config" {
  provider = google-beta

  secret_id = data.terraform_remote_state.secret_manager.outputs.cc_api_gateway_qa_config_secret_id
  role      = "roles/secretmanager.secretAccessor"
  member    = "serviceAccount:${google_service_account.cloud_run.email}"
}

# Allow access to cc-api-gateway-us config
resource "google_secret_manager_secret_iam_member" "cloud_run_cc_api_gateway_us_config" {
  provider = google-beta

  secret_id = data.terraform_remote_state.secret_manager.outputs.cc_api_gateway_us_config_secret_id
  role      = "roles/secretmanager.secretAccessor"
  member    = "serviceAccount:${google_service_account.cloud_run.email}"
}

# Allow access to cc-api-gateway-sg config
resource "google_secret_manager_secret_iam_member" "cloud_run_cc_api_gateway_sg_config" {
  provider = google-beta

  secret_id = data.terraform_remote_state.secret_manager.outputs.cc_api_gateway_sg_config_secret_id
  role      = "roles/secretmanager.secretAccessor"
  member    = "serviceAccount:${google_service_account.cloud_run.email}"
}

# Allow access to cc-api-gateway-eu config
resource "google_secret_manager_secret_iam_member" "cloud_run_cc_api_gateway_eu_config" {
  provider = google-beta

  secret_id = data.terraform_remote_state.secret_manager.outputs.cc_api_gateway_eu_config_secret_id
  role      = "roles/secretmanager.secretAccessor"
  member    = "serviceAccount:${google_service_account.cloud_run.email}"
}

# Allow access to cc-editor-api-qa config
resource "google_secret_manager_secret_iam_member" "cloud_run_cc_editor_api_qa_config" {
  provider = google-beta

  secret_id = data.terraform_remote_state.secret_manager.outputs.cc_editor_api_qa_config_secret_id
  role      = "roles/secretmanager.secretAccessor"
  member    = "serviceAccount:${google_service_account.cloud_run.email}"
}

# Allow access to cc-editor-api-us config
resource "google_secret_manager_secret_iam_member" "cloud_run_cc_editor_api_us_config" {
  provider = google-beta

  secret_id = data.terraform_remote_state.secret_manager.outputs.cc_editor_api_us_config_secret_id
  role      = "roles/secretmanager.secretAccessor"
  member    = "serviceAccount:${google_service_account.cloud_run.email}"
}

# Allow access to cc-editor-api-sg config
resource "google_secret_manager_secret_iam_member" "cloud_run_cc_editor_api_sg_config" {
  provider = google-beta

  secret_id = data.terraform_remote_state.secret_manager.outputs.cc_editor_api_sg_config_secret_id
  role      = "roles/secretmanager.secretAccessor"
  member    = "serviceAccount:${google_service_account.cloud_run.email}"
}

# Allow access to cc-editor-api-eu config
resource "google_secret_manager_secret_iam_member" "cloud_run_cc_editor_api_eu_config" {
  provider = google-beta

  secret_id = data.terraform_remote_state.secret_manager.outputs.cc_editor_api_eu_config_secret_id
  role      = "roles/secretmanager.secretAccessor"
  member    = "serviceAccount:${google_service_account.cloud_run.email}"
}

# Allow access to cc-api-qa config
resource "google_secret_manager_secret_iam_member" "cloud_run_cc_api_qa_config" {
  provider = google-beta

  secret_id = data.terraform_remote_state.secret_manager.outputs.cc_api_qa_config_secret_id
  role      = "roles/secretmanager.secretAccessor"
  member    = "serviceAccount:${google_service_account.cloud_run.email}"
}

# Allow access to cc-api config
resource "google_secret_manager_secret_iam_member" "cloud_run_cc_api_config" {
  provider = google-beta

  secret_id = data.terraform_remote_state.secret_manager.outputs.cc_api_config_secret_id
  role      = "roles/secretmanager.secretAccessor"
  member    = "serviceAccount:${google_service_account.cloud_run.email}"
}

# Allow access to cc-lti-qa config
resource "google_secret_manager_secret_iam_member" "cloud_run_cc_lti_qa_config" {
  provider = google-beta

  secret_id = data.terraform_remote_state.secret_manager.outputs.cc_lti_qa_config_secret_id
  role      = "roles/secretmanager.secretAccessor"
  member    = "serviceAccount:${google_service_account.cloud_run.email}"
}

# Allow access to cc-lti config
resource "google_secret_manager_secret_iam_member" "cloud_run_cc_lti_config" {
  provider = google-beta

  secret_id = data.terraform_remote_state.secret_manager.outputs.cc_lti_config_secret_id
  role      = "roles/secretmanager.secretAccessor"
  member    = "serviceAccount:${google_service_account.cloud_run.email}"
}

//                                            $$\ $$\                                                                                       $$\
//                                            \__|$$ |                                                                                      $$ |
// $$$$$$$\  $$$$$$\  $$$$$$\$$$$\   $$$$$$\  $$\ $$ | $$$$$$\   $$$$$$\         $$$$$$$\  $$$$$$\   $$$$$$\ $$\    $$\ $$$$$$\  $$$$$$$\ $$$$$$\
//$$  _____|$$  __$$\ $$  _$$  _$$\ $$  __$$\ $$ |$$ |$$  __$$\ $$  __$$\       $$  _____|$$  __$$\ $$  __$$\\$$\  $$  |\____$$\ $$  __$$\\_$$  _|
//$$ /      $$ /  $$ |$$ / $$ / $$ |$$ /  $$ |$$ |$$ |$$$$$$$$ |$$ |  \__|      \$$$$$$\  $$$$$$$$ |$$ |  \__|\$$\$$  / $$$$$$$ |$$ |  $$ | $$ |
//$$ |      $$ |  $$ |$$ | $$ | $$ |$$ |  $$ |$$ |$$ |$$   ____|$$ |             \____$$\ $$   ____|$$ |       \$$$  / $$  __$$ |$$ |  $$ | $$ |$$\
//\$$$$$$$\ \$$$$$$  |$$ | $$ | $$ |$$$$$$$  |$$ |$$ |\$$$$$$$\ $$ |            $$$$$$$  |\$$$$$$$\ $$ |        \$  /  \$$$$$$$ |$$ |  $$ | \$$$$  |
// \_______| \______/ \__| \__| \__|$$  ____/ \__|\__| \_______|\__|            \_______/  \_______|\__|         \_/    \_______|\__|  \__|  \____/
//                                  $$ |
//                                  $$ |
//                                  \__|

resource "google_service_account" "cc_compiler_servant" {
  account_id   = "cc-compiler-servant"
  display_name = "Compiler Servant"
  description  = "Service account for Compute Engine Instances running cc-compiler-servant"
}

resource "google_project_iam_member" "cc_compiler_servant_monitoring_metricwriter" {
  role   = "roles/monitoring.metricWriter"
  member = "serviceAccount:${google_service_account.cc_compiler_servant.email}"
}

//          $$\
//          $$ |
// $$$$$$\  $$ |  $$\  $$$$$$\
//$$  __$$\ $$ | $$  |$$  __$$\
//$$ /  $$ |$$$$$$  / $$$$$$$$ |
//$$ |  $$ |$$  _$$<  $$   ____|
//\$$$$$$$ |$$ | \$$\ \$$$$$$$\
// \____$$ |\__|  \__| \_______|
//$$\   $$ |
//\$$$$$$  |
// \______/

resource "google_service_account" "cc_share" {
  account_id   = "cc-share"
  display_name = "CodeCollab ShareDB"
  description  = "Service Account for cc-share running on GKE"
}

# Allow access to cc-share-qa config
resource "google_secret_manager_secret_iam_member" "gke_cc_share_qa_config" {
  provider = google-beta

  secret_id = data.terraform_remote_state.secret_manager.outputs.cc_share_qa_config_secret_id
  role      = "roles/secretmanager.secretAccessor"
  member    = "serviceAccount:${google_service_account.cc_share.email}"
}

# Allow access to cc-share-us config
resource "google_secret_manager_secret_iam_member" "gke_cc_share_us_config" {
  provider = google-beta

  secret_id = data.terraform_remote_state.secret_manager.outputs.cc_share_us_config_secret_id
  role      = "roles/secretmanager.secretAccessor"
  member    = "serviceAccount:${google_service_account.cc_share.email}"
}

# Allow access to cc-share-sg config
resource "google_secret_manager_secret_iam_member" "gke_cc_share_sg_config" {
  provider = google-beta

  secret_id = data.terraform_remote_state.secret_manager.outputs.cc_share_sg_config_secret_id
  role      = "roles/secretmanager.secretAccessor"
  member    = "serviceAccount:${google_service_account.cc_share.email}"
}

# Allow access to cc-share-eu config
resource "google_secret_manager_secret_iam_member" "gke_cc_share_eu_config" {
  provider = google-beta

  secret_id = data.terraform_remote_state.secret_manager.outputs.cc_share_eu_config_secret_id
  role      = "roles/secretmanager.secretAccessor"
  member    = "serviceAccount:${google_service_account.cc_share.email}"
}

resource "google_service_account" "cc_compiler_master" {
  account_id   = "cc-compiler-master"
  display_name = "CodeCollab Compiler Master"
  description  = "Service Account for cc-compiler-master running on GKE"
}

# Allow access to cc-compiler-master-qa config
resource "google_secret_manager_secret_iam_member" "gke_cc_compiler_master_qa_config" {
  provider = google-beta

  secret_id = data.terraform_remote_state.secret_manager.outputs.cc_compiler_master_qa_config_secret_id
  role      = "roles/secretmanager.secretAccessor"
  member    = "serviceAccount:${google_service_account.cc_compiler_master.email}"
}

# Allow access to cc-compiler-master-us config
resource "google_secret_manager_secret_iam_member" "gke_cc_compiler_master_us_config" {
  provider = google-beta

  secret_id = data.terraform_remote_state.secret_manager.outputs.cc_compiler_master_us_config_secret_id
  role      = "roles/secretmanager.secretAccessor"
  member    = "serviceAccount:${google_service_account.cc_compiler_master.email}"
}

# Allow access to cc-compiler-master-sg config
resource "google_secret_manager_secret_iam_member" "gke_cc_compiler_master_sg_config" {
  provider = google-beta

  secret_id = data.terraform_remote_state.secret_manager.outputs.cc_compiler_master_sg_config_secret_id
  role      = "roles/secretmanager.secretAccessor"
  member    = "serviceAccount:${google_service_account.cc_compiler_master.email}"
}

# Allow access to cc-compiler-master-eu config
resource "google_secret_manager_secret_iam_member" "gke_cc_compiler_master_eu_config" {
  provider = google-beta

  secret_id = data.terraform_remote_state.secret_manager.outputs.cc_compiler_master_eu_config_secret_id
  role      = "roles/secretmanager.secretAccessor"
  member    = "serviceAccount:${google_service_account.cc_compiler_master.email}"
}

resource "google_service_account" "cc_html_server" {
  account_id   = "cc-html-server"
  display_name = "CodeCollab HTML Server"
  description  = "Service Account for cc-html-server running on GKE"
}

# Allow access to cc-compiler-master-qa config
resource "google_secret_manager_secret_iam_member" "gke_cc_html_server_qa_config" {
  provider = google-beta

  secret_id = data.terraform_remote_state.secret_manager.outputs.cc_html_server_qa_config_secret_id
  role      = "roles/secretmanager.secretAccessor"
  member    = "serviceAccount:${google_service_account.cc_html_server.email}"
}

# Allow access to cc-compiler-master-us config
resource "google_secret_manager_secret_iam_member" "gke_cc_html_server_us_config" {
  provider = google-beta

  secret_id = data.terraform_remote_state.secret_manager.outputs.cc_html_server_us_config_secret_id
  role      = "roles/secretmanager.secretAccessor"
  member    = "serviceAccount:${google_service_account.cc_html_server.email}"
}

# Allow access to cc-compiler-master-sg config
resource "google_secret_manager_secret_iam_member" "gke_cc_html_server_sg_config" {
  provider = google-beta

  secret_id = data.terraform_remote_state.secret_manager.outputs.cc_html_server_sg_config_secret_id
  role      = "roles/secretmanager.secretAccessor"
  member    = "serviceAccount:${google_service_account.cc_html_server.email}"
}

# Allow access to cc-compiler-master-eu config
resource "google_secret_manager_secret_iam_member" "gke_cc_html_server_eu_config" {
  provider = google-beta

  secret_id = data.terraform_remote_state.secret_manager.outputs.cc_html_server_eu_config_secret_id
  role      = "roles/secretmanager.secretAccessor"
  member    = "serviceAccount:${google_service_account.cc_html_server.email}"
}

resource "google_service_account_iam_member" "workload_identity_cc_share" {
  service_account_id = google_service_account.cc_share.name
  role               = "roles/iam.workloadIdentityUser"
  member             = "serviceAccount:${data.google_project.project.project_id}.svc.id.goog[cc-share/cc-share]"
}

resource "google_service_account_iam_member" "workload_identity_cc_html_server" {
  service_account_id = google_service_account.cc_html_server.name
  role               = "roles/iam.workloadIdentityUser"
  member             = "serviceAccount:${data.google_project.project.project_id}.svc.id.goog[cc-html-server/cc-html-server]"
}

resource "google_service_account_iam_member" "workload_identity_cc_compiler_master" {
  service_account_id = google_service_account.cc_compiler_master.name
  role               = "roles/iam.workloadIdentityUser"
  member             = "serviceAccount:${data.google_project.project.project_id}.svc.id.goog[cc-compiler-master/cc-compiler-master]"
}

