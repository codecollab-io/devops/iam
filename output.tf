output "gitlab_ci_account" {
  value = google_service_account.gitlab_ci.email
}

output "gitlab_ci_key" {
  value     = google_service_account_key.gitlab_ci.private_key
  sensitive = true
}


output "cloud_run_account" {
  value = google_service_account.cloud_run.email
}

output "cc_compiler_servant_account" {
  value = google_service_account.cc_compiler_servant.email
}

output "cc_share_account" {
  value = google_service_account.cc_share.email
}

output "cc_compiler_master_account" {
  value = google_service_account.cc_compiler_master.email
}

output "cc_html_server_account" {
  value = google_service_account.cc_html_server.email
}
