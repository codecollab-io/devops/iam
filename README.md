## IAM

Terraform configuration for IAM. This configuration handles

- Service Accounts on Google Cloud
- IAM Policies on Google Cloud

### Note

Roles should not be configured with Terraform due to Google Cloud's soft deletion causing potential issues. Instead,
create roles from the console and use them in the configuration.